<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed'); 
	
	/**
	@Module:		Programs
	@Name:			edit_program_activity.php
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Hanterar redigering av en programaktivitet.
					
	@History
	DATUM			VEM						ÅTGÄRD
	2015-03-09		Kalle Henriksson		Skapade filen	
	
	*/

?>
<h1><?PHP echo lang('programs_activity_headline'); ?></h1>
<?PHP  


	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	// Feedback
	echo $this->session->flashdata('message');
	
	echo validation_errors(); 

	$has_ep = isset($program) && count($program) == 1;

	$epa = $program_activity[0];

	$epa_id								=	$epa->epa_id;
	$epa_ep_id							=	$epa->epa_ep_id;
	$epa_ea_id							=	$epa->epa_ea_id;
	$epa_published						=	$epa->epa_published;
	$epa_created						=	$epa->epa_created;
	$epa_created_by						=	$epa->epa_created_by;
	$epa_updated						=	$epa->epa_updated;
	$epa_updated_by						=	$epa->epa_updated_by;
	$epa_status							=	$epa->epa_status;
	$epa_status_reason					=	$epa->epa_status_reason;
	$epa_start_time						=	$epa->epa_start_time;
	$epa_end_time						=	$epa->epa_end_time;
	$epac_id							=	$epa->epac_id;
	$epac_title							=	$epa->epac_title;
	$epac_description					=	$epa->epac_description;
	$epac_seo_keywords					=	$epa->epac_seo_keywords;
	$epac_seo_description				=	$epa->epac_seo_description;
	
?>

<link rel="stylesheet" type="text/css" href="<?PHP echo BASE_URL; ?>third-party/modules/programs/js/bootstrap-datetimepicker/bootstrap-datetimepicker.css">
<script src="<?PHP echo BASE_URL; ?>third-party/modules/programs/js/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

<script type="text/javascript">
	(function($){
		$.fn.datetimepicker.dates['sv-SE'] = {
			days: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag", "Söndag"],
			daysShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör", "Sön"],
			daysMin: ["S", "M", "T", "O", "T", "F", "L", "S"],
			months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
			today: "Idag"
		};
	}(jQuery));

	$(function(){
		$('#datetimepicker').datetimepicker({
			weekStart: 1,
			format: 'yyyy-MM-dd hh:mm',
			language: 'sv-SE',
			pickSeconds: false,
		});
	});
</script>

<div class="content_box">

	<div class="box_header">
		<h4><?PHP echo lang('programs_edit_program_activity'); ?></h4>
    </div>
    
    <div class="box_content">
    	<?php 
			
			// Startar formuläret
			echo form_open('admin/programs/save_program_activity', array('class'=>'form label-top'));
			
		?>
			<input type="hidden" name="epa_id" value="<?php echo $epa_id; ?>" />
			<input type="hidden" name="epac_id" value="<?php echo $epac_id; ?>">

            <!-- TITEL -->
    		<div class="field">
    			<label class="required" for="epac_title"><?PHP echo lang('system_title'); ?></label>
    			<input type="text" class="xlarge" maxlength="50" name="epac_title" id="epac_title" value="<?php if($this->session->flashdata('epac_title')) { echo $this->session->flashdata('epac_title');} else { echo $epac_title; } ?>" />
    		</div>
            
            <!-- BESKRIVNING -->
    		<div class="field">
    			<label class="required" for="epac_description"><?PHP echo lang('system_description'); ?></label>
    			<textarea class="wysiwyg xlarge" rel="tiny" cols="50" rows="7" name="epac_description" id="epac_description"><?php if($this->session->flashdata('epac_description')) { echo $this->session->flashdata('epac_description');} else { echo $epac_description; } ?></textarea>
    		</div>

    		<div class="field">
    			<div id="datetimepicker" class="input-append date">
			      <input readonly="readonly" type="text"></input>
			      <span class="add-on"><i data-time-icon="fa fa-clock-o" data-date-icon="fa fa-calendar"></i></span>
			    </div>
    		</div>
            
    		<div class="field">
	            <!-- STARTDATUM -->
	    		<div class="floater" style="margin-right: 15px;">
	    			<label class="required" for="epa_start_time"><?PHP echo lang('programs_activity_start_time'); ?></label>
	    			<input type="text" class="medium text jdpicker" maxlength="20" name="epa_start_time" id="epa_start_time" value="<?php if($this->session->flashdata('epa_start_time')) { echo $this->session->flashdata('epa_start_time');} else { echo $epa_start_time; } ?>" />	    			
	    		</div>
	            
	            <!-- SLUTDATUM -->
	    		<div class="floater">
	    			<label class="required" for="epa_end_time"><?PHP echo lang('programs_activity_end_time'); ?></label>
	    			<input type="text" class="medium text jdpicker" maxlength="20" name="epa_end_time" id="epa_end_time" value="<?php if($this->session->flashdata('epa_end_time')) { echo $this->session->flashdata('epa_end_time');} else { echo $epa_end_time; } ?>"  />
	    		</div>
    		</div>

            <!-- SEO-NYCKELORD -->
    		<div class="field">
    			<label class="" for="epac_seo_keywords"><?PHP echo lang('programs_seo_keywords'); ?></label>
    			<input type="text" class="xlarge" maxlength="50" name="epac_seo_keywords" id="epac_seo_keywords" value="<?php if($this->session->flashdata('epac_seo_keywords')) { echo $this->session->flashdata('epac_seo_keywords');} else { echo $epac_seo_keywords; } ?>" />
    		</div>
                        
            <!-- SEO-BESKRIVNING -->
    		<div class="field">
    			<label class="" for="epac_seo_description"><?PHP echo lang('programs_seo_description'); ?></label>
    			<textarea  class="xlarge" rel="tiny" cols="50" rows="7" name="epac_seo_description" id="epac_seo_description"><?php if($this->session->flashdata('epac_seo_description')) { echo $this->session->flashdata('epac_seo_description');} else { echo $epac_seo_description; } ?></textarea>
    		</div>



            <!-- PUBLICERAD -->
    		<div class="field">
    			<label class="required" for="epa_published"><?PHP echo lang('system_published'); ?></label>
				<select id="epa_published" name="epa_published" class="medium">
				    <option value="1" <?php echo ($epa_published == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('system_published') ?></option>
				    <option value="0" <?php echo ($epa_published == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('system_draft') ?></option>
				</select>
    		</div>


            <!-- STATUS -->
    		<div class="field">
    			<label class="required" for="epa_status"><?PHP echo lang('system_published'); ?></label>
				<select id="epa_status" name="epa_status" class="medium">
				    <option value="2" <?php echo ($epa_status == 2) ? 'selected="selected"' : ''; ?>><?php echo lang('programs_status_canceled') ?></option>
				    <option value="1" <?php echo ($epa_status == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('programs_status_active') ?></option>
				    <option value="0" <?php echo ($epa_status == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('programs_status_removed') ?></option>
				</select>
    		</div>
                        
            <!-- ORSAK TILL STATUS -->
    		<div class="field">
    			<label class="" for="epa_status_reason"><?PHP echo lang('programs_activity_status_reason'); ?></label>
    			<textarea  class="" cols="50" rows="3" name="epa_status_reason" id="epa_status_reason"><?php if($this->session->flashdata('epa_status_reason')) { echo $this->session->flashdata('epa_status_reason');} else { echo $epa_status_reason; } ?></textarea>
    		</div>


            <!-- SPARA -->
       		<div class="buttonrow">
       			<input type="submit" name="save" id="save" value="<?PHP echo lang('system_save'); ?>" class="btn" />
    		</div>
            
    	</form>
    </div>
</div>