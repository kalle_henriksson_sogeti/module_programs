<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed'); 
	
	/**
	@Module:		Programs
	@Name:			edit_program.php
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Hanterar redigering av ett program.
					
	@History
	DATUM			VEM						ÅTGÄRD
	2015-03-09		Kalle Henriksson		Skapade filen	
	
	*/

?>
<h1><?PHP echo lang('programs_headline'); ?></h1>
<?PHP  


	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	// Feedback
	echo $this->session->flashdata('message');
	
	echo validation_errors(); 

	$has_ep = isset($program) && count($program) == 1;

	$ep_id								=	($has_ep ? $program[0]->ep_id : "");
	$ep_event_id						=	($has_ep ? $program[0]->ep_event_id : "");
	$ep_published						=	($has_ep ? $program[0]->ep_published : "");
	$ep_created							=	($has_ep ? $program[0]->ep_created : "");
	$ep_created_by						=	($has_ep ? $program[0]->ep_created_by : "");
	$ep_updated							=	($has_ep ? $program[0]->ep_updated : "");
	$ep_updated_by						=	($has_ep ? $program[0]->ep_updated_by : "");
	$ep_status							=	($has_ep ? $program[0]->ep_status : "");
	$epc_id								=	($has_ep ? $program[0]->epc_id : "");
	$epc_title							=	($has_ep ? $program[0]->epc_title : "");
	$epc_description					=	($has_ep ? $program[0]->epc_description : "");
	$epc_seo_keywords					=	($has_ep ? $program[0]->epc_seo_keywords : "");
	$epc_seo_description				=	($has_ep ? $program[0]->epc_seo_description : "");
	
?>


<link rel="stylesheet" href="<?PHP echo BASE_URL; ?>third-party/modules/programs/js/multi-select/css/multi-select.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?PHP echo BASE_URL; ?>third-party/modules/programs/js/multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript">
//	function initProgramActivitiesMultiselect(){
//		if($('#programActivitiesSelectList').size() > 0){
//			$('#programActivitiesSelectList').multiSelect({
//				selectableHeader: "<label style='background-color:transparent;'><?php echo lang('programs_available_activities'); ?></label>",
//				selectionHeader: "<label><?php echo lang('programs_selected_activities'); ?></label>",
//			});
//		}
//	}
</script>

<div class="content_box">

	<div class="box_header">
		<?php if ($has_ep) { ?>
			<h4><?PHP echo lang('programs_edit_program'); ?></h4>
		<?php } else { ?>
	    	<h4><?PHP echo lang('programs_add_program'); ?></h4>
		<?php } ?>
    </div>
    
    <div class="box_content">
    	<?php 
			
			// Startar formuläret
//    		if($has_ep){
				echo form_open('admin/programs/save_program', array('class'=>'form label-top'));
//    		}else{
//				echo form_open('admin/programs/create_program', array('class'=>'form label-top'));
//    		}
			
		?>
			<input type="hidden" name="ep_id" value="<?php echo $ep_id; ?>" />
			<!-- <input type="hidden" name="ep_status" value="<?php /*echo $ep_status;*/ ?>" /> -->
			<input type="hidden" name="epc_id" value="<?php echo $epc_id; ?>">

            <!-- TITEL -->
    		<div class="field">
    			<label class="required" for="epc_title"><?PHP echo lang('system_title'); ?></label>
    			<input type="text" class="xlarge" maxlength="50" name="epc_title" id="epc_title" value="<?php if($this->session->flashdata('epc_title')) { echo $this->session->flashdata('epc_title');} else { echo $epc_title; } ?>" />
    		</div>
            
            <!-- BESKRIVNING -->
    		<div class="field">
    			<label class="required" for="epc_description"><?PHP echo lang('system_description'); ?></label>
    			<textarea class="wysiwyg xlarge" rel="tiny" cols="50" rows="7" name="epc_description" id="epc_description"><?php if($this->session->flashdata('epc_description')) { echo $this->session->flashdata('epc_description');} else { echo $epc_description; } ?></textarea>
    		</div>

            <!-- SEO-NYCKELORD -->
    		<div class="field">
    			<label class="" for="epc_seo_keywords"><?PHP echo lang('programs_seo_keywords'); ?></label>
    			<input type="text" class="xlarge" maxlength="50" name="epc_seo_keywords" id="epc_seo_keywords" value="<?php if($this->session->flashdata('epc_seo_keywords')) { echo $this->session->flashdata('epc_seo_keywords');} else { echo $epc_seo_keywords; } ?>" />
    		</div>
                        
            <!-- SEO-BESKRIVNING -->
    		<div class="field">
    			<label class="" for="epc_seo_description"><?PHP echo lang('programs_seo_description'); ?></label>
    			<textarea  class="xlarge" rel="tiny" cols="50" rows="7" name="epc_seo_description" id="epc_seo_description"><?php if($this->session->flashdata('epc_seo_description')) { echo $this->session->flashdata('epc_seo_description');} else { echo $epc_seo_description; } ?></textarea>
    		</div>











    		<!-- VAL AV EVENT ATT KOPPLA PROGRAMMET TILL -->
    		<div class="field">
    			<label class="" for="ep_event_id"><?PHP echo lang('programs_events'); ?></label>
				<select id="ep_event_id" name="ep_event_id"  class="xlarge">
				<option value="-1"></option>
					<?PHP
					// Listar alla event i systemet
			    	  foreach ($events as $post) { ?>
			    		<option value="<?PHP echo $post->event_id; ?>" <?PHP echo ($ep_event_id == $post->event_id) ? 'selected="selected"' : ''; ?>>
						<?PHP echo $post->ec_title; ?>
	                    </option>
			    	<?php } ?>
			    </select>
    		</div>







    		<!-- SCRIPT FÖR ATT LÄGGA TILL ACTIVITET TILL PROGRAMMET -->
    		<script type="text/javascript">
	    		$(function(){
					$('#add_activity_to_program').on('click', function(){
						if($('#programActivitiesSelectList').val() < 0){
							$('#programActivitiesSelectList').focus();
							return false;
						}

						$.ajax({
							type: 'POST',
							url: "<?PHP echo BASE_URL; ?>admin/programs/program_activities_action",
							data: {
								'activity_id': $('#programActivitiesSelectList').val(),
								'ep_id' : <?php echo $ep_id; ?>,
								'do[]' : ['add_activity_to_program',],
							},
							dataType: 'text',
							complete: function(data){
								location.reload();
							}
						});

 					});	    			
	    		});
    		</script>


    		<!-- VISA ALLA VALBARA AKTIVITETER UR AKTIVITETSBIBLIOTEKET -->
    		<div class="field">
				<label><?php echo lang('programs_available_activities'); ?></label>
				<div class="floater">
					<select id="programActivitiesSelectList" name="item[]" class="xlarge">
						<option value="-1"></option>
					<?PHP
					// Listar alla event i systemet
			    	  foreach ($activities as $post) { ?>
			    		<option value="<?PHP echo $post->ea_id; ?>">
						<?PHP echo $post->eac_title; ?>
	                    </option>
			    	<?php } ?>
					</select>
				</div>
				<div class="floater" style="margin-left: 10px; margin-top: -1px;">
					<input type="button" id="add_activity_to_program" value="Lägg till" />
				</div>
    		</div>


    		<!-- VISA ALLA REDAN KOPPLADE AKTIVITETER -->
    		<div class="field">
				<label><?php echo lang('programs_selected_activities'); ?></label>
	    		<table cellpadding="0" cellspacing="0" width="100%" class="data_table">
	            	<thead>
	                    <tr>
	                        <th width="10" class="no-sort"><input type="checkbox" class="check_all" title="Check all" /></th>
	                        <th><?PHP echo lang('system_title'); ?></th>
	                        <th>Starttid</th>
	                        <th>Sluttid</th>
	                        <th>Beskrivning</th>
	                        <th class="no-sort">&nbsp;</th>
	                        <th class="no-sort">&nbsp;</th>
	                    </tr>
	                </thead>
					<?PHP 
	                
	                    foreach($selected_activities as $post)
	                    { 
	                    
	                ?>
	                    <tr id="page_<?PHP echo $post->epa_ea_id; ?>">
	                        <td><input type="checkbox" name="item[]" value="<?PHP echo $post->epa_ea_id; ?>" /></td>
	                        <td><?PHP echo $post->epac_title; ?></td>
	                        <td><?PHP echo ($post->epa_start_time == "0000-00-00 00:00:00" ? ("[" .lang('programs_not_set'). "]") : $post->epa_start_time); ?></td>
	                        <td><?PHP echo ($post->epa_end_time == "0000-00-00 00:00:00" ? ("[" .lang('programs_not_set'). "]") : $post->epa_end_time); ?></td>
	                        <td><div class="clip-text-single-row-ellipsis"><?PHP echo strip_tags($post->epac_description); ?></div></td>
	                        <td><?PHP echo anchor("admin/programs/edit_program_activity/" . $post->epa_id, lang('system_edit')); ?></td>
	                        <td><?PHP echo anchor("admin/programs/delete_program_activity/" . $post->epa_id, lang('system_delete')); ?></td>
	                    </tr>
	                <?PHP 
	                
	                    } 
	                    
	                ?>
				</table>
    		</div>












            <!-- PUBLICERAD -->
    		<div class="field">
    			<label class="required" for="ep_published"><?PHP echo lang('system_published'); ?></label>
				<select id="ep_published" name="ep_published" class="medium">
				    <option value="1" <?php echo ($ep_published == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('system_published') ?></option>
				    <option value="0" <?php echo ($ep_published == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('system_draft') ?></option>
				</select>
    		</div>


            <!-- LÄGG TILL -->
       		<div class="buttonrow">
       			<input type="submit" name="save" id="save" value="<?PHP echo lang('system_save'); ?>" class="btn" />
    		</div>
            
    	</form>
    </div>
</div>