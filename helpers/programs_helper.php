<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');

	
	
	/**
	@Module:		Modules
	@Name:			modules_helper.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Luckylane, Johan Lindqvist
	@Created:		2014
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna fil inneh�ller gemensamma funktioner f�r hanteringen av moduler p� webbplatsen.
	
	@History
	DATE			AUTHOR				ACTION
	2014-10-14		Johan Lindqvist		Skapade modulen.
		
	*/




	/**
	@Name:			_programs_get_programs_data($language, $published, $status=null, $event_id=null)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Funktionen h�mtar in alla program baserat p� spr�k, publicerad-flagga, status-flagga och event-id.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
    function _programs_get_programs_data($language, $published=0, $status=null, $event_id=null)
	{
		$ci = & get_instance();
		
		//	Filtrera p� angivet spr�k
		$ci->db->join('events_programs_content','events_programs_content.epc_ep_id = events_programs.ep_id');
		$ci->db->where('events_programs_content.epc_language', $language);

		$ci->db->join('events','events.event_id = events_programs.ep_event_id');
		$ci->db->join('events_content','events_content.ec_event_id = events_programs.ep_event_id');
		
		//	Filtrera p� publicerad
		if($published)
		{			
			$ci->db->where('events_programs.ep_published', $published);
		}
		
		//	Filtrera p� konto-id
		if($event_id != null)
		{			
			$ci->db->where('events_programs.ep_event_id', $event_id);
		}
		
		//	Filtrera p� status
		if($status != null){
			$ci->db->where('events_programs.ep_status', $status);
		}

		$query	=	$ci->db->get('events_programs');

		return $query;

	}







	/**
	@Name:			_programs_get_program_data($program_id, $language)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$program_id, $language
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Funktionen h�mtar in ett program baserat p� spr�k och program-id.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
	function _programs_get_program_data($program_id, $language){
		$ci = & get_instance();

		$ci->db->where('events_programs.ep_id', $program_id);
		$ci->db->join('events_programs_content','events_programs_content.epc_ep_id = events_programs.ep_id');
		$ci->db->where('events_programs_content.epc_language', $language);

		$query = $ci->db->get('events_programs');
		return $query;
	}








	/**
	@Name:			_programs_get_events_programs_activities_data($program_id, $language)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$program_id, $language
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
	function _programs_get_events_programs_activities_data($program_id, $language){
		$ci = & get_instance();

		$ci->db->where('events_programs_activities.epa_ep_id', $program_id);
		$ci->db->join('events_programs_activities_content','events_programs_activities_content.epac_epa_id = events_programs_activities.epa_id');

		$ci->db->where('events_programs_activities_content.epac_language', $language);

		$query = $ci->db->get('events_programs_activities');

		return $query;
	}









	/**
	@Name:			_programs_get_events_programs_activity_data($program_id, $language)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$program_id, $language
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
	function _programs_get_events_programs_activity_data($epa_id, $language){
		$ci = & get_instance();

		$ci->db->where('events_programs_activities.epa_id', $epa_id);
		$ci->db->join('events_programs_activities_content','events_programs_activities_content.epac_epa_id = events_programs_activities.epa_id');

		$ci->db->where('events_programs_activities_content.epac_language', $language);

		$query = $ci->db->get('events_programs_activities');

		return $query;
	}




	

	
?>