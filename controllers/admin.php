<?PHP
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	if (!defined('BASEPATH')) exit('No direct script access allowed');	
	
/**
@Module:		Programs
@Name:			admin.php
--------------------------------------------------------------------------------------------------
@Creator:		Soget, Kalle Henriksson
@Created:		2015
@Version:		1.0
@PHP Version: 	5	
--------------------------------------------------------------------------------------------------
@Description	Denna fil hanterar modulen för eventprogram.

@History
DATE			AUTHOR				ACTION
2014-03-09		Kalle Henriksson		Skapade modulen.
	
*/
class Admin extends Admin_Controller 
{				  
	
	function __construct()
	{
		
		parent::Admin_Controller();
		
		// Hämtar in nödvändiga filer
		
		// Läser in libraries
		$this->load->library(array(
			'form_validation',
		));
			
		// Läser in models
		$this->load->model(array(
			'language_model',
			'events/events_model',
			'activities/activities_model',
			'programs/programs_model',
			'contact/contact_model',
			'accounts/accounts_model',
		));
		
		// Läser in helpers
		$this->load->helper(array(
			'language',
			'url',
			'events/events',
			'activities/activities',
			'programs',
			'contact/contact',
			'form',
			'accounts/accounts',
		));
		
	}
	
	/**
	@Name:			function index()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			none
	
	@Return								
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Hämtar in alla registrerade program och skriver ut dessa i en lista.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
	function index()
	{
		// Hämtar in registrerade moduler.
		$data['programs']	=	$this->programs_model->get_all_programs();
		
		// Skriver ut vyn där modulerna listas upp i en tabell.
		$this->template->build('programslist', $data);	
	}







	
	/**
	@Name:			add_program()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att registrera ett nytt program i systemet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function add_program()
	{
		//	Hämta event så att man kan koppla ett program mot ett event
		$data['events']		= $this->events_model->get_all_events();

		// Skriver ut vyn med ett formulär för att registrera ett nytt program i systemet			
		$this->template->build('edit_program', $data);
	}






	/**
	@Name:			edit_program()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att redigera ett program.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function edit_program(){

/*		$selectedActivitiesIds = array();
		$selectedActivitiesIds[] = 2;
		$selectedActivitiesIds[] = 3;
*/

		// Sätter ID för aktuellt program
		$program_id	=	$this->uri->segment(4);
		
		//	Hämta eventlista så att man kan koppla ett program mot ett event
		$data['events']						= $this->events_model->get_all_events();

		//	Hämta alla aktiviteter som finns att använda
		$data['activities']					= $this->activities_model->get_activities();

		// Hämtar programmet från DB
		$data['program'] 					= $this->programs_model->get_program($program_id);

		//	Hämta alla kopplingar mot aktiviteter som finns på engivet program
		$data['selected_activities']		= $this->programs_model->get_program_activities($program_id);



/*		//	Ta fram id för kopplade aktiviteter
		foreach ($selectedActivities as $sa) {
			$selectedActivitiesIds[] = $sa->epa_ea_id;
		}

		//	Leverera kopplade aktiviteters id:n till vyn
		$data['selected_activities_ids'] 	=	$selectedActivitiesIds;

*/


		// Skriver ut vyn med ett formulär för att redigera ett program			
		$this->template->build('edit_program', $data);
	}












	/**
	@Name:			edit_program_activity()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			

	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att redigera en programaktivitet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-13		Kalle Henriksson	Skapade funktionen
		
	*/
	function edit_program_activity(){

		// Sätter ID för aktuell programaktivitet
		$epa_id	=	$this->uri->segment(4);
		
		//	Hämta aktiviteten
		$data['program_activity']	= $this->programs_model->get_program_activity($epa_id);



/*		//	Ta fram id för kopplade aktiviteter
		foreach ($selectedActivities as $sa) {
			$selectedActivitiesIds[] = $sa->epa_ea_id;
		}

		//	Leverera kopplade aktiviteters id:n till vyn
		$data['selected_activities_ids'] 	=	$selectedActivitiesIds;

*/


		// Skriver ut vyn med ett formulär för att redigera ett program			
		$this->template->build('edit_program_activity', $data);
	}








	/**
	@Name:			save_program_activity()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			

	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Sparar en programaktivitet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-13		Kalle Henriksson	Skapade funktionen
		
	*/
	function save_program_activity(){

		// Läser in aktiva språk för att kunna registrera/uppdatera posten på samtliga aktiva språk
		$langs = $this->language_model->get_available_languages();
		$epa_id = $this->input->post('epa_id');
		$activity_ids = $this->input->post('programActivitiesSelectList');

		// Sätter variabler för posten
		$data = array(
			'epa_start_time' 	=> $this->input->post('epa_start_time'),
			'epa_end_time' 		=> $this->input->post('epa_end_time'),
			'epa_published' 	=> $this->input->post('epa_published'),
			'epa_updated' 		=> date("Y-m-d H:i:s"),
			'epa_updated_by' 	=> $this->session->userdata('user_id'),
		);
		
		if($epa_id != ""){
			// Uppdaterar posten		
			$this->db->where('epa_id', $epa_id)->update('events_programs_activities', $data);

			
			// TABELLEN EVENTS_CONTENT
			// Uppdaterar postens innehåll (i tabellen events_content) för varje språk...
			foreach($langs as $lang)
			{				
				// Sätter variabler
				$epac_id					=	$this->input->post('epac_id');
				$epac_title					=	$this->input->post('epac_title');
				$epac_description			=	$this->input->post('epac_description');
				$epac_seo_keywords			=	$this->input->post('epac_seo_keywords');
				$epac_seo_description		=	$this->input->post('epac_seo_description');
				$epac_language				=	$lang->language_code;
							
				$ec = array(
				   'epac_title' 			=> $epac_title,
				   'epac_description' 		=> $epac_description,
				   'epac_seo_keywords' 		=> $epac_seo_keywords,
				   'epac_seo_description' 	=> $epac_seo_description,
				   'epac_language'			=> $epac_language
				);				

				// Har vi ett epac_id skall vi UPPDATERA posten
				if($epac_id != "") {								
					$this->db->where('epac_id', $epac_id);				
					$this->db->update('events_programs_activities_content', $ec); 
									
				} else {
					$this->db->insert('events_programs_activities_content', $ec);
				}
			}

			// Feedback
			$this->session->set_flashdata('msg', '<div class="message success"><p>Aktiviteten är sparad.</p></div>');
		}else{
			// Feedback
			$this->session->set_flashdata('msg', '<div class="message error"><p>Något gick fel.</p></div>');
		}


		// Visar vyn igen
		redirect('admin/programs', 'refresh');
	}











	/**
	@Name:			save_program()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Hanterar både uppdatering av befintligt samt skapande av nytt program och returnerar en vy.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen
		
	*/
	function save_program(){

		// Läser in aktiva språk för att kunna registrera/uppdatera posten på samtliga aktiva språk
		$langs = $this->language_model->get_available_languages();
		$ep_id = $this->input->post('ep_id');
		$activity_ids = $this->input->post('programActivitiesSelectList');

		// Sätter variabler för posten
		$data = array(
			'ep_published' 				=> $this->input->post('ep_published'),
			'ep_updated' 				=> date("Y-m-d H:i:s"),
			'ep_updated_by' 			=> $this->session->userdata('user_id'),
			'ep_event_id'				=> $this->input->post('ep_event_id'),
		);
		
		if($ep_id != ""){
			// Uppdaterar posten		
			$this->db->where('ep_id', $ep_id)->update('events_programs', $data);
		}else{
			$data["ep_created"] = date("Y-m-d H:i:s");
			$data["ep_created_by"] = $this->session->userdata('user_id');
			$data["ep_status"] = 1;
			
			// Sparar ny post		
			$this->db->insert('events_programs', $data);
			
			// Sätter id för den post vi just skapade
			$ep_id = $this->db->insert_id();
		}


		
		// TABELLEN EVENTS_CONTENT
		// Uppdaterar postens innehåll (i tabellen events_content) för varje språk...
		foreach($langs as $lang)
		{
			
			// Sätter variabler
			$epc_id						=	$this->input->post('epc_id');
			$epc_title					=	$this->input->post('epc_title');
			$epc_description			=	$this->input->post('epc_description');
			$epc_seo_keywords			=	$this->input->post('epc_seo_keywords');
			$epc_seo_description		=	$this->input->post('epc_seo_description');
			$epc_language				=	$lang->language_code;
						

			$ec = array(
			   'epc_ep_id' 				=> $ep_id,
			   'epc_title' 				=> $epc_title,
			   'epc_description' 		=> $epc_description,
			   'epc_seo_keywords' 		=> $epc_seo_keywords,
			   'epc_seo_description' 	=> $epc_seo_description,
			   'epc_language'			=> $epc_language
			);				

			// Har vi ett epc_id skall vi UPPDATERA posten
			if($epc_id != "") {								
				$this->db->where('epc_id', $epc_id);				
				$this->db->update('events_programs_content', $ec); 
								
			} else {
				$this->db->insert('events_programs_content', $ec);
			}
		}




		//	START - HANTERA PROGRAMAKTIVITETER...


		$pActivities = $this->db->get('events_programs_activities');

		//	Gå igenom alla önskade aktivitets-idn att koppla
		foreach ($activity_ids as $activity_id) {
			//	Kolla så
		}

		//	SLUT - HANTERA PROGRAMAKTIVITETER...





		// Feedback
		$this->session->set_flashdata('msg', '<div class="message success"><p>Programmet är sparat.</p></div>');
		
		// Visar vyn igen
		redirect('admin/programs', 'refresh');
	}



	


	/**
	@Name:			function programs_action()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Return			object					
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Administrerar ett program. Används uteslutande av systemadministratören.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
	
	function programs_action() {

		$do 	= $this->input->post('do');
		$item 	= $this->input->post('item');
		
		// Skall vi ta bort poster. Observera att vi inte raderar posterna ännu.
		// Vi sätter _status till 0 för att kunna ångra oss.
		if(isset($do['delete']))
		{			
			foreach($item as $id)
			{
				$this->db->where('ep_id', $id)->update('events_programs', array('ep_status' => '0'));
			}
			
			// Feedback.
			$this->session->set_flashdata('msg', '<div class="message success">Markerade program inaktiverades.</div>');
		
		} elseif(isset($do['change_published'])) {

			// Skall vi byta status (läst/oläst) på posterna gör vi det nu.
			foreach($item as $id)
			{
				$this->db->where('ep_id', $id)->update('events_programs', array('ep_published' => $this->input->post('ep_published')));
			}
			
			// Feedback.
			$this->session->set_flashdata('message', '<div class="message success">Publiceringsstatus bytt</div>');		
		}
		
		// Läser in vyn igen.
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}


	/**
	@Name:			function events_action()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Return			object					
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Administrerar ett event. Används uteslutande av systemadministratören.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen.
		
	*/
	
	function program_activities_action() {


		$ep_id 			= $this->input->post('ep_id');
		$do 			= $this->input->post('do');
		$activity_id 	= $this->input->post('activity_id');

		$langs 			= $this->language_model->get_available_languages();



		// Skall vi ta bort poster. Observera att vi inte raderar posterna ännu.
		// Vi sätter _status till 0 för att kunna ångra oss.
		if(in_array('delete', $do))
		{			
/*			foreach($item as $id)
			{
				$this->db->where('event_id', $id)->update('events', array('event_status' => '0'));
			}
			
			// Feedback.
			$this->session->set_flashdata('msg', '<div class="message success">Valda aktiviteter borttagna.</div>');
		*/
		} elseif(in_array('add_activity_to_program', $do)) {
			// Ta fram activities och koppla dom till program genom att skapa events_programs_activities
//			foreach($item as $id)
//			{
				$activity = $this->activities_model->get_activity($activity_id);

				if($activity == null){
					die("W00t?!");
				}


				$program_activity = array(
					"epa_ep_id" 			=> $ep_id,
					"epa_ea_id" 			=> $activity_id,
					"epa_published" 		=> 0,
					"epa_created" 			=> date("Y-m-d H:i:s"),
					"epa_created_by" 		=> $this->session->userdata('user_id'),
					"epa_updated" 			=> date("Y-m-d H:i:s"),
					"epa_updated_by" 		=> $this->session->userdata('user_id'),
					"epa_status" 			=> 1,
					"epa_status_reason" 	=> "",
				);

				$this->db->insert('events_programs_activities', $program_activity);
				$epa_id = $this->db->insert_id();

				//	Skapa det språkberoende datat
				foreach($langs as $lang)
				{				
					$content = array(
					   'epac_epa_id' 				=> $epa_id,
					   'epac_title' 				=> $activity->eac_title,
					   'epac_description' 			=> $activity->eac_description,
					   'epac_seo_keywords' 			=> $activity->eac_seo_keywords,
					   'epac_seo_description' 		=> $activity->eac_seo_description,
					   'epac_language'				=> $activity->eac_language,
					);

					$this->db->insert('events_programs_activities_content', $content);
				}
//			}


			
			// Feedback.
			$this->session->set_flashdata('message', '<div class="message success">Aktivitet tillagt.</div>');		
		}else{
			$this->session->set_flashdata('message', '<div class="message success">Något gick fel.</div>');		
		}
//		
//		// Läser in vyn igen.
//		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}		

}

?>