<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	
	/**
	@Module:		Programs
	@Name:			programs_lang.php
	@Language:		Svenska
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Denna fil hanterar språkvariablar för modulen.
	
	@History
	DATUM			VEM					ÅTGÄRD
	2015-03-09		Kalle Henriksson	Skapade filen.
	
	*/
	
	$lang['programs_module_name']					=	"programs";
	$lang['programs_headline']						=	"Program";
	$lang['programs_activity_headline']				=	"Programaktivitet";
	$lang['programs_add_program']					=	"Lägg till program";
	$lang['programs_edit_program']					=	"Redigera program";
	$lang['programs_edit_program_activity']			=	"Redigera programaktivitet";
	$lang['programs_activity_start_time']			=	"Starttid";
	$lang['programs_activity_end_time']				=	"Sluttid";
	$lang['programs_activity_status_reason']		=	"Orsak till vald status";
	$lang['programs_title']							=	"Programnamn";
	$lang['programs_program_was_saved']				=	"Programmet har sparats";
	$lang['programs_seo_keywords']					=	"SEO -nyckelord";
	$lang['programs_seo_description']				=	"SEO -beskrivning";
	$lang['programs_events']						=	"Event";
	$lang['programs_event']							=	"Event";
	$lang['programs_available_activities']			=	"Tillgängliga aktiviteter";
	$lang['programs_selected_activities']			=	"Valda aktiviteter";
	$lang['programs_not_set']						=	"Ej satt";
	$lang['programs_status_active']					=	"Aktiv";
	$lang['programs_status_canceled']				=	"Inställd";
	$lang['programs_status_removed']				=	"Borttagen";

?>