<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	
/**
@Module:		Programs
@Name:			programs_model.php
--------------------------------------------------------------------------------------------------
@Creator:		Sogeti, Kalle Henriksson
@Created:		2015
@Version:		1.0
@PHP Version: 	5	
--------------------------------------------------------------------------------------------------
@Description	Denna fil hanterar programmodulen.
				
@History
DATE			AUTHOR				ACTION
2015-02-23		Kalle Henriksson		Skapade modulen.
	
*/
class Programs_model extends MY_Model 
{
	
	function __construct()
    {
       
	    parent::Model();
		
    }




    
	
	/**
	@Name:			get_all_programs()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla program för ett visst program. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson		Skapade funktionen
		
	*/
    function get_all_programs($language='sv')
	{	
		$query = _programs_get_programs_data($language)->result();
		
		return $query;
	}	





    
	
	/**
	@Name:			get_programs()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$language, $published, $status, $account_id
					
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla program som inte är borttagna, för ett visst språk. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
	*/
    function get_programs($language='sv', $account_id = null)
	{	
		$query = _programs_get_programs_data($language, null, 1, $account_id)->result();
		
		return $query;
	}





    
	
	/**
	@Name:			get_program()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$program_id, $language
		
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar ett program för ett visst språk baserat på program_id. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
		
	*/
	function get_program($program_id, $language='sv'){
		$query = _programs_get_program_data($program_id, $language)->result();
		
		return $query;
	}








	/**
	@Name:			get_program_activities()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$program_id, $language
		
	@return			result_array()
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar en lista med kopplade aktiviteter för ett visst program. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-21		Kalle Henriksson	Skapade funktionen
		
	*/
	function get_program_activities($program_id, $language='sv'){
		$query = _programs_get_events_programs_activities_data($program_id, $language)->result();

		return $query;
	}










	/**
	@Name:			get_program_activities()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$program_id, $language
		
	@return			result_array()
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar en lista med kopplade aktiviteter för ett visst program. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-21		Kalle Henriksson	Skapade funktionen
		
	*/
	function get_program_activity($epa_id, $language='sv'){
		$query = _programs_get_events_programs_activity_data($epa_id, $language)->result();

		return $query;
	}

} 